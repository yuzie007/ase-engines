import pytest
import numpy as np

def test_dos_files_and_shapes(si_dos):
    properties = si_dos.read_results()

    assert properties.get('eigenvalues', None) is not None, 'No eigenvalues returned.'
    assert properties.get('ibz_kpoints', None) is not None, 'No k-points returned.'
    assert properties.get('nband', None) is not None, 'No number of bands returned.'

    eigenvalues = properties['eigenvalues']
    ibz_kpoints = properties['ibz_kpoints']
    nband = properties['nband']

    assert eigenvalues.shape[1] == len(ibz_kpoints); 'Returned eigenvalues do not match the number of k-points in the bandpath.'
    assert eigenvalues.shape[2] == nband; 'Returned eigenvalues do not match the number of bands.'


def test_dos_eigenvalues(si_dos):
    properties = si_dos.read_results()
    eigenvalues = properties['eigenvalues']
    np.testing.assert_allclose(eigenvalues[0, 0, 0], -5.47217686, atol=1e-5)
