import pytest

import numpy as np

from ase.io.aims import read_aims_output
from ase.spectrum.band_structure import BandStructure

from engines.aims.aims import AimsEngine, AimsConfig
from pathlib import Path


def pytest_namespace():
    return {"gs": None}


@pytest.mark.dependency()
def test_groundstate(atoms, parameters, tmpdir, mock_execute):
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    mock_execute(atoms, parameters, tmpdir)
    pytest.gs = engine.groundstate(
        atoms,
        parameters,
        tmpdir,
        ["energy", "forces", "stress"],
    )

    copy_dir = Path(tmpdir) / "aims_copy"
    pytest.gs.copy_elsi_restart(copy_dir)

    assert len(list(copy_dir.glob("*.csc"))) > 0
    with pytest.raises(FileExistsError):
        pytest.gs.copy_elsi_restart(copy_dir)

    pytest.gs.copy_elsi_restart(Path(copy_dir), True)

    read_atoms = read_aims_output(f"{tmpdir}/aims.out")

    assert np.sum(np.abs(read_atoms.get_forces().flatten())) < 1e-7
    assert int(np.round(read_atoms.get_potential_energy())) == -15696
    assert np.allclose(
        read_atoms.get_stress(), [0.00337464, 0.00337465, 0.00337465, 0.0, 0.0, 0.0]
    )
    # test not overwriting the calc in a dir
    with pytest.raises(IOError):
        engine.groundstate(atoms, parameters, tmpdir)


@pytest.mark.dependency(depends=["test_groundstate"])
def test_bandstructure(atoms, parameters, tmpdir, mock_execute):
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    bandpath = atoms.cell.bandpath(density=5)

    bs = engine.bandstructure(
        pytest.gs,
        parameters,
        Path(tmpdir) / "bandstructure",
        bandpath,
    )

    results = bs.read_aims_bands()
    assert len(bandpath.kpts) == len(results["kpts"])
    maxerr = abs(bandpath.kpts - results["kpts"]).max()
    assert maxerr < 1e-5

    bs = BandStructure(bandpath, results["eigenvalues"])
    bs.write(tmpdir / "bs.json")


def test_bandstructure_single_shot(atoms, parameters, tmpdir, mocker):
    configuration = AimsConfig()
    engine = AimsEngine(configuration)

    bandpath = atoms.cell.bandpath(density=5)

    bs = engine.bandstructure_single_shot(
        atoms,
        parameters,
        tmpdir,
        bandpath,
    )

    results = bs.read_aims_bands()
    assert len(bandpath.kpts) == len(results["kpts"])
    maxerr = abs(bandpath.kpts - results["kpts"]).max()
    assert maxerr < 1e-5

    bs = BandStructure(bandpath, results["eigenvalues"])
    bs.write(tmpdir / "bs.json")
