import pytest

from ase.build import bulk
from ase.atom import Atom

from engines.aims.aims import AimsEngine, AimsConfig, ase2aims_bandpath
from pathlib import Path
import os


@pytest.fixture
def atoms():
    return bulk("NaCl", "rocksalt", a=4.5)


def test_check_directory(tmpdir):
    configuartion = AimsConfig()
    engine = AimsEngine(configuartion)
    directory = engine.check_directory(tmpdir, False)
    assert directory.exists()

    # Testing should already exist, but without an aims.out file so engine.check_directory should not fail.
    assert engine.check_directory(tmpdir, False) == tmpdir

    file = open(f"{directory}/aims.out", "w")
    file.write("testing\n")
    file.close()

    # Testing should already exist and aims.out exists, so engine.check_directory should fail.
    with pytest.raises(FileExistsError):
        engine.check_directory(tmpdir, False)

    # Testing should already exist, but overwrite is True so engine.check_directory should not fail.
    assert engine.check_directory(tmpdir, True) == tmpdir


def test_ase2aims_bandpath(atoms):
    bandpath = atoms.cell.bandpath(density=5)
    bands = ase2aims_bandpath(bandpath)
    expected_bands = [
        "band   0.00000  0.00000  0.00000   0.50000  0.00000  0.50000    7 G  X  ",
        "band   0.50000  0.00000  0.50000   0.50000  0.25000  0.75000    4 X  W  ",
        "band   0.50000  0.25000  0.75000   0.37500  0.37500  0.75000    3 W  K  ",
        "band   0.37500  0.37500  0.75000   0.00000  0.00000  0.00000    8 K  G  ",
        "band   0.00000  0.00000  0.00000   0.50000  0.50000  0.50000    7 G  L  ",
        "band   0.50000  0.50000  0.50000   0.62500  0.25000  0.62500    5 L  U  ",
        "band   0.62500  0.25000  0.62500   0.50000  0.25000  0.75000    3 U  W  ",
        "band   0.50000  0.25000  0.75000   0.50000  0.50000  0.50000    6 W  L  ",
        "band   0.50000  0.50000  0.50000   0.37500  0.37500  0.75000    6 L  K  ",
        "band   0.62500  0.25000  0.62500   0.50000  0.00000  0.50000    4 U  X  ",
    ]

    for band, expected_band in zip(bands, expected_bands):
        assert band == expected_band


def test_make_profile():
    configuartion_explicit = AimsConfig("testing the commands")
    assert all(
        [
            arg == expected_arg
            for arg, expected_arg in zip(
                configuartion_explicit.profile.argv, ["testing", "the", "commands"]
            )
        ]
    )

    original_ase_aims_command = os.environ.pop("ASE_AIMS_COMMAND", None)
    configuration_default = AimsConfig()
    assert (configuration_default.profile.argv[0] == "aims.x") and (
        len(configuration_default.profile.argv) == 1
    )

    os.environ["ASE_AIMS_COMMAND"] = "srun aims.x"
    configuration_default_set_env = AimsConfig()
    assert (
        (configuration_default_set_env.profile.argv[1] == "aims.x")
        and (configuration_default_set_env.profile.argv[0] == "srun")
        and (len(configuration_default_set_env.profile.argv) == 2)
    )

    if original_ase_aims_command is None:
        del os.environ["ASE_AIMS_COMMAND"]
    else:
        os.environ["ASE_AIMS_COMMAND"] = original_ase_aims_command


def test_fetch_species(atoms, tmpdir):
    original_species_dir = os.environ["AIMS_SPECIES_DIR"]
    del os.environ["AIMS_SPECIES_DIR"]

    with pytest.raises(ValueError):
        configuration_failure = AimsConfig()

    os.environ["AIMS_SPECIES_DIR"] = original_species_dir

    base_species_direc = Path(__file__).parent / "testdata" / "species_defaults"
    config = AimsConfig(basis_set_direc=base_species_direc)
    config_light = AimsConfig(basis_set_direc=base_species_direc / "light")
    config_tight = AimsConfig(basis_set_direc=base_species_direc / "tight")
    new_atoms = atoms.copy()
    new_atoms.append(Atom("H", [0.0, 2.0, 0.0]))
    assert config.fetch_species(atoms) == base_species_direc / "light"
    species_parameters = {}

    assert (
        config_light.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "light"
    )

    assert (
        config_light.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "light"
    )

    species_parameters["default"] = "light"
    assert (
        config.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "light"
    )

    with pytest.raises(FileExistsError):
        config.fetch_species(
            new_atoms,
            species_parameters=species_parameters,
        )

    species_parameters["default"] = "tight"
    assert (
        config.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "tight"
    )

    assert (
        config.fetch_species(
            new_atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "tight"
    )

    species_parameters["Na"] = "tight"
    assert (
        config.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "tight"
    )

    species_parameters["Cl"] = "tight"
    assert (
        config.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "tight"
    )

    del species_parameters["default"]
    assert (
        config.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "tight"
    )

    del species_parameters["Cl"]
    assert (
        config.fetch_species(
            atoms,
            species_parameters=species_parameters,
        )
        == base_species_direc / "tight"
    )

    species_parameters["Cl"] = "light"
    assert (
        config.fetch_species(
            atoms,
            working_direc=tmpdir,
            species_parameters=species_parameters,
        )
        == tmpdir / "basissets"
    )

    assert (
        config.fetch_species(
            new_atoms,
            working_direc=tmpdir,
            species_parameters=species_parameters,
        )
        == tmpdir / "basissets"
    )

    species_parameters["Cl"] = "tight"
    species_parameters["Na"] = "light"

    with pytest.raises(FileExistsError):
        config.fetch_species(
            new_atoms,
            species_parameters=species_parameters,
        )

    species_parameters["H"] = "light"

    with pytest.raises(FileExistsError):
        config.fetch_species(
            new_atoms,
            species_parameters=species_parameters,
        )

    species_parameters["H"] = "tight"
    assert (
        config.fetch_species(
            new_atoms,
            working_direc=tmpdir,
            species_parameters=species_parameters,
        )
        == tmpdir / "basissets"
    )
