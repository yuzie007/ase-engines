from pathlib import Path

import pytest
from ase.calculators.espresso import EspressoProfile

from engines.espresso.espresso import Espresso


@pytest.fixture(scope='session')
def espresso(config):
    exe = config['executables'].get('espresso')
    if exe is None:
        pytest.skip(reason='espresso not configured')
    return Espresso(EspressoProfile(argv=[exe]))


@pytest.fixture(scope='session')
def atoms():
    from ase.build import bulk
    # Used by many tests, TODO unduplicate
    return bulk('Si')


@pytest.fixture(scope='session')
def pseudopotentials():
    # Returns a selection of UPF pseudopotentials
    # as a dictionary of espresso keywords.
    asetest = pytest.importorskip('asetest')
    paths = asetest.datafiles.paths['espresso']
    pseudo_dir = paths[0]

    pseudopotentials = {}
    for ppath in pseudo_dir.glob('*.UPF'):
        fname = ppath.name
        symbol = fname.split('_', 1)[0].capitalize()
        pseudopotentials[symbol] = fname

    assert pseudopotentials

    # The espresso writer wants to receive strings and not Path objects.
    return {'pseudo_dir': str(pseudo_dir),
            'pseudopotentials': pseudopotentials}


@pytest.fixture(scope='session')
def groundstate(espresso, atoms, tmp_path_factory, pseudopotentials):
    # Ugly pasted paths.  Improve pseudopotential path handling

    kwargs = dict(
        kpts=[4, 4, 4],
        input_data={'control': {}},
        ecutwfc=600 / 27.211)

    kwargs.update(pseudopotentials)

    return espresso.groundstate(
        atoms=atoms,
        parameters=kwargs,
        directory=tmp_path_factory.mktemp('espresso_gs'))


def test_groundstate(groundstate):
    results = groundstate.read_results()
    assert 'energy' in results


def test_bandstructure(espresso, groundstate, tmp_path):
    atoms = groundstate.atoms.copy()
    bandpath = atoms.cell.bandpath('GX', npoints=6)
    bscalc = espresso.bandstructure(
        gs=groundstate, bandpath=bandpath,
        parameters={}, directory=tmp_path)

    results = bscalc.read_results()
    eigenvalues = results['eigenvalues']
    espresso_kpts = results['ibz_kpoints']

    kpts_err = abs(bandpath.kpts - espresso_kpts).max()
    # Parsed kpoints should deviate from input kpoints by less
    # than printing precision:
    assert kpts_err < 1e-5
    # from ase.spectrum.band_structure import BandStructure
    # XXX we also should get the Fermi level
    # bs = BandStructure(bandpath, eigenvalues)
    # bs.plot(show=True)
