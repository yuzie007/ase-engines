from pathlib import Path
import shutil
from typing import Optional

from ase import Atoms
from ase.calculators.vasp import Vasp

class VaspCalculation:
    """
    Class to run vasp calculations. Should eventually use a template and a
    profile. For now just uses the old vasp calculator.

    Can copy output files to a new directory for follow-up calculations
    and run vasp in a work directory.
    """
    def __init__(self, atoms: Atoms, parameters: dict, directory: Path):
        self.atoms = atoms
        self.parameters = parameters
        self.directory = directory

    def copy_car_files(self, directory):
        # What will happen if the files are not there?
        files = ['WAVECAR', 'CHGCAR', 'CHG', 'CONTCAR']
        for file in files:
            shutil.copyfile(self.directory / file, directory / file)

    def run(self):
        calc = Vasp(atoms=self.atoms, directory=self.directory, **self.parameters)
        self.atoms.calc = calc
        self.atoms.get_potential_energy()


def groundstate(atoms: Atoms,
                parameters: dict,
                directory: Path) -> VaspCalculation:
    """
    Runs a groundstate calculation with VASP using the VaspCalculation class

    Parameters
    ----------
    atoms : ase.atoms.Atoms
        An ASE atoms object
    parameters : dict
        input parameters for a vasp calculation. Includes not only INCAR
        parameters, but also kpts, etc.
    directory : pathlib.PosixPath
        Path to the working directory.

    Returns
    -------
    calc : VaspCalculation
        The vasp calculation containing the atoms object with the results.
    """
    directory.mkdir(exist_ok=True, parents=True)

    # Force an actual ground state calculation, i.e. set NSW=0, etc.
    # parameters['nsw'] = 0 

    calc = VaspCalculation(atoms, parameters, directory)
    calc.run()
    return calc

def bandstructure(gs: VaspCalculation,
                  bandpath: str,
                  parameters: dict,
                  directory: Path,
                  total_kpoints: int=100) -> VaspCalculation:
    """
    

    Parameters
    ----------
    gs : VaspCalculation
        Groundstate calculation
    bandpath : str
        String containing the high symmetry points of the kpath. E.g. 'LGXU'
        WARNING! FOR NOW DOES NOT WORK FOR NON-CONTINOUS PATHS e.g. LGXU,KG!!
    parameters : dict
        Calculation parameters
    directory : Path
        Work directory.
    total_kpoints : int, optional
        Total number of kpoints for all the high-symmetry directions.
        The default is 100.

    Returns
    -------
    VaspCalculation
        Bandstructure calculation including the atoms object containing the
        results.
    """
    atoms = gs.atoms.copy()

    directory.mkdir(exist_ok=True, parents=True)

    gs.copy_car_files(directory)

    bsparameters = {
        **gs.parameters,
        #kpts': bandpath.kpts,
        'kpts': {'path': bandpath, 'npoints': total_kpoints},
        # Forcing ICHARG=11, and ismear=0. Anything else (EDIFF, SIGMA,...)?
        'icharg': 11,
        'ismear': 0,
        **parameters}

    calc = VaspCalculation(atoms, bsparameters, directory)
    calc.run()
    return calc

def relaxation(atoms: Atoms,
               parameters: dict,
               directory: Path,
               target: Optional[str] = "full",
               isif: Optional[int] = None,
               fmax: Optional[float] = None,
               emax: float = 1e-5,
               max_iterations: int = 64) -> VaspCalculation:
    """
    Relax the given Atoms object using the VaspCalculation class

    What gets relaxed depends on the input and can be a predefined list of
    relaxation targets, which are stated under the "target" parameter.

    Parameters
    ----------
    atoms : ase.atoms.Atoms
        An ASE atoms object
    parameters : dict
        Input parameters for a vasp calculation. Includes not only INCAR
        parameters, but also kpts, etc.
    directory : pathlib.PurePath
        Path to the working directory.
    target : str, optional
        A keyword that explains which parameters to relax - position, unit
        cell shape, unit cell volume. Must be one of the following:
            positions, cell_shape, volume, positions_and_cell_shape,
            cell_shape_and_volume, full
    isif : int, optional
        The VASP integer that sets the relaxation parameters.
    fmax : float, optional
        Perform the relaxation until the maximum force on any atom is
        below this threshold. The unit is eV/Å.
    emax : float, optional
        Perform the relaxation until the energy difference between two
        subsequent ionic steps are below this threshold. The value is in
        eV.
    max_interations : int
        The maximum number of ionic steps to relax for.

    Returns
    -------
    calc : VaspCalculation
        The vasp calculation containing the relaxed atoms object with the
        results.

    Raises
    ------
    ValueError
        If neither "target" or "isif" is given, or if neither "fmax" or
        "emax" is given.

    Notes
    -----
        Explicitly setting "isif" will always override the setting of
        "target".
        If both "fmax" and "emax" are given, "fmax" will take precedence.
    """
    # The possibilities for isif with the given keywords
    isif_converter = {
        "positions": 2,
        "cell_shape": 5,
        "volume": 7,
        "positions_and_cell_shape": 4,
        "cell_shape_and_volume": 6,
        "full": 3
    }

    # Is only None if isif is not given and target is not any of the options
    _isif = isif or isif_converter.get(target.lower())

    if _isif is None:
        raise ValueError(
            "Input parameter \"target\" must be one of:\n" +
            "\n".join([f"    \"{k}\"" for k in isif_converter])
        )

    # Can't be a simple or, since negation doesn't work on None
    ediffg = -fmax if fmax is not None else emax

    # Make the dict with the processing from this function and allow it to be
    # overwritten by the key-values of the parameters input
    params = dict(
        isif=_isif,
        ediffg=ediffg,
        ibrion=2,
        nsw=max_iterations
    )
    params.update(parameters)

    directory.mkdir(exist_ok=True, parents=True)

    calc = VaspCalculation(atoms, params, directory)
    calc.run()
    return calc


def main():
    from ase.build import bulk
    directory = Path('work/vasp')
    directory.mkdir(exist_ok=True, parents=True)
    
    atoms = bulk('Si', crystalstructure='fcc', a=3.9)

    rlx = relaxation(atoms,
                     {'xc': 'PBE',
                      'encut': 240,
                      'kpts': [15, 15, 15]},
                     directory / 'relaxation')
    atoms = rlx.atoms

    gs = groundstate(atoms,
                     {'kpts': [15, 15, 15],
                      'nsw': 0,
                      'xc': 'PBE',
                      'encut': 240,
                      'ismear': -5},
                     directory)
    print(gs.atoms.get_potential_energy())

    bandstructure_directory = directory / 'bandstructure'
    bandpath = 'LGXU'
    bscalc = bandstructure(gs, bandpath,
                           {'xc': 'PBE', 'encut': 240},
                           bandstructure_directory, 80)
    bs = bscalc.atoms.calc.band_structure()
    efermi = bscalc.atoms.calc.get_fermi_level()
    bs.plot(emin=-15+efermi, emax=15+efermi, filename='Si_bandstructure.png')
    
if __name__ == '__main__':
    main()
